<?php

namespace Lanuma\kly;

use Illuminate\Support\ServiceProvider;

class NewshubServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the apllication services
     *
     * @return viod
     */
    public function boot()
    {
        // publish newshub config
        $this->publishes([
            __DIR__ . DIRECTORY_SEPARATOR . 'config/newshub.php' => config_path('newshub.php'),
        ], 'newshub-config');

        // publish model
        $this->publishes([
            __DIR__ . DIRECTORY_SEPARATOR . 'models/' => app_path(),
        ], 'newshub-models');

        // publish artisan command
        $this->publishes([
            __DIR__ . DIRECTORY_SEPARATOR . 'Commands/' => app_path('Console/Commands'),
        ], 'newshub-command');

        // load package migrations
        $this->loadMigrationsFrom(__DIR__ . DIRECTORY_SEPARATOR . 'migrations');

    }

    /**
     * Register package services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__ . DIRECTORY_SEPARATOR . 'config/newshub.php', 'newshub'
        );
    }
}
