<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->engine = 'MyISAM';
            
            $table->integer('id')->unsigned();
            $table->primary('id');
            $table->string('title');
            $table->text('description');
            $table->dateTime('schedule_start');
            $table->dateTime('schedule_end');
            $table->string('place')->nullable();
            $table->string('image');
            $table->string('full_url');
            $table->string('url')->nullable();
            $table->string('flag')->nullable();
            $table->string('type');
            $table->text('tag')->nullable();
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
