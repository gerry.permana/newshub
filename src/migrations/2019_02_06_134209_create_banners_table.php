<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banner', function (Blueprint $table) {
            $table->engine = 'MyISAM';
            
            $table->integer('id')->unsigned;
            $table->primary('id');
            $table->string('title');
            $table->datetime('schedule_start');
            $table->datetime('schedule_end');
            $table->string('wording')->nullable();
            $table->text('image')->nullable();
            $table->string('url')->nullable();
            $table->string('type', 10)->nullable();
            $table->boolean('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banner');
    }
}
