<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->engine = 'MyISAM';
            
            $table->integer('id');
            $table->primary('id');
            $table->string('name');
            $table->string('url');
            $table->string('youtube');
            $table->string('instagram');
            $table->string('facebook');
            $table->string('twitter');
            $table->string('image');
            $table->string('image_cover');
            $table->boolean('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
