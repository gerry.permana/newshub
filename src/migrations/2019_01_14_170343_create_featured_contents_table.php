<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeaturedContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('featured_contents', function (Blueprint $table) {
            $table->engine = 'MyISAM';
            
            $table->integer('id')->unsigned();
            $table->primary('id');
            $table->unsignedTinyInteger('order_position');
            $table->string('order_name');
            $table->dateTime('schedule_start')->index('schedule_start');
            $table->dateTime('schedule_end')->index('schedule_end');
            $table->string('type_tag', 32);
            $table->string('tag');
            $table->text('news');
            $table->string('title');
            $table->string('url');
            $table->mediumText('desc');
            $table->string('image');
            $table->string('image_secondary');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('featured_contents');
    }
}
