<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Lanuma\Kly\Newshub;
use Lanuma\Kly\Newshub\Log;
use App\Videos;

class NewshubVideos extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'newshub:videos
                            {--i|id= : video id}
                            {--p|page= : Number of page that will be shown. This won\'t work if max_id parameter is exist.}
                            {--l|limit= : Total rows per page}
                            {--t|last_update= : String datetime or integer timestamp.}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Retrieve videos from newshub';

    /**
     * Instance Newshub
     */
    protected $newshub;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Newshub $newshub)
    {
        parent::__construct();
        $this->newshub = $newshub;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(Log $log)
    {
        $id         = $this->option('id');
        $page       = $this->option('page');
        $limit      = $this->option('limit');
        $lastUpdate = $this->option('last_update');

        $checkRunning = $log->check_running_status(Log::SECTION_VIDEO);

        if ($checkRunning !== null) {
            return $this->error('Sorry the system is still working, wait another minute... !');
        }

        $lastSuccess = $log->check_last_success(Log::SECTION_VIDEO);
        $lastSuccess = ($lastSuccess !== null) ? (string) strtotime($lastSuccess->created_at) : '';
        $time        = (!empty($lastUpdate)) ? $lastUpdate : $lastSuccess;

        if(!empty($id)) {
            $this->newshub->id($id);
        }else{
            $this->newshub->videos()
                          ->page($page)
                          ->limit($limit)
                          ->last_update($time);
        }

        // create new log
        $logId = $log->create_running_log(Log::SECTION_VIDEO);

        $success = 0;
        $fail = array();
        $total = 0;
        $is_next_page = true;
        while($is_next_page) {
            // get video
            $getVideo = $this->newshub->get();

            if($getVideo === null) {
                $is_next_page = false;
                $fail['message'] = 'Error while get video';
                $fail['error'] = $getVideo;
                $status = Log::FAIL;
            } else {
                $status = Log::SUCCESS;
                $data = array_key_exists('data', $getVideo) ? $getVideo->data : array($getVideo);
                $attribut = array_key_exists('attributes', $getVideo) ? $getVideo->attributes : array();

                // belum diganti
                foreach($data as $video) {
                    $videoModel = Videos::updateOrCreate([
                        'id' => $video->news_id
                    ],[
                        'news_title' => $video->news_title ,
                        'news_subtitle' => $video->news_subtitle ,
                        'news_synopsis' => $video->news_synopsis ,
                        'news_content' => $video->news_content ,
                        'news_image_real' => $video->news_image->real ,
                        'news_image_thumbnail' => $video->news_image_thumbnail->real ,
                        'news_image_potrait' => $video->news_image_potrait->real ,
                        'news_image_headline' => $video->news_image_headline ,
                        'news_date_publish' => $video->news_date_publish ,
                        'news_type' => $video->news_type ,
                        'news_editor' => json_encode($video->news_editor) ,
                        'news_paging' => json_encode($video->news_paging) ,
                        'category' => json_encode($video->news_category) ,
                        'news_url_full' => $video->news_url_full ,
                        'news_url' => $video->news_url ,
                        'news_video' => json_encode($video->news_video) ,
                        'tag_id' => $video->news_tag['0']->tag_id ,
                        'news_tag' => json_encode($video->news_tag) ,

                    ]);

                    if($videoModel) {
                        $success++;
                    }else{
                        $fail['message'] = 'Error while save video';
                        $fail['error'] = $videoModel;
                    }

                    $total++;
                }

                if(isset($attribut->next_page)){
                    $is_next_page = true;
                    $page = $attribut->next_page;
                    $this->newshub->page($page);
                }else{
                    $is_next_page = false;
                }
            }
        }

        // report
        $log->update_log($logId,[
            // 'section' => Log::SECTION_BANNER,
            'total' => $total,
            'total_success' => $success,
            // 'total_fail' => ,
            'log_fail' => json_encode($fail),
            'ref_url' => $this->newshub->get_uri(),
            'status' => Log::SUCCESS,
        ]);

        $this->info("Get videos Success !");
        $headers = ['total data', 'success', 'fails', 'ref url'];
        $tables = [$total, $success, json_encode($fail), $this->newshub->get_uri()];
        $this->table($headers, [$tables]);
    }
}