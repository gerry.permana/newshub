<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Lanuma\Kly\Newshub;
use Lanuma\Kly\Newshub\Log;
use App\FeaturedContent;

class NewshubWhatshot extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'newshub:featuredContent
                            {--i|id= : featured content id}
                            {--p|page= : Number of page that will be shown. This won\'t work if max_id parameter is exist.}
                            {--l|limit= : Total rows per page}
                            {--t|last_update= : String datetime or integer timestamp.}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Retrieve featured content from newshub';

    /**
     * Instance Newshub
     */
    protected $newshub;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Newshub $newshub)
    {
        parent::__construct();
        $this->newshub = $newshub;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(Log $log)
    {
        $id         = $this->option('id');
        $page       = $this->option('page');
        $limit      = $this->option('limit');
        $lastUpdate = $this->option('last_update');

        $checkRunning = $log->check_running_status(Log::SECTION_FEATURE);

        if ($checkRunning !== null) {
            return $this->error('Sorry the system is still working, wait another minute... !');
        }

        $lastSuccess = $log->check_last_success(Log::SECTION_FEATURE);
        $lastSuccess = ($lastSuccess !== null) ? (string) strtotime($lastSuccess->created_at) : '';
        $time        = (!empty($lastUpdate)) ? $lastUpdate : $lastSuccess;

        if(!empty($id)) {
            $this->newshub->id($id);
        }else{
            $this->newshub->featured_content()
                          ->page($page)
                          ->limit($limit)
                          ->last_update($time);
        }

        // create new log
        $logId = $log->create_running_log(Log::SECTION_FEATURE);

        $success = 0;
        $fail = array();
        $total = 0;
        $is_next_page = true;
        while($is_next_page) {

            $getFeaturedContent = $this->newshub->get();

            if($getFeaturedContent === null) {
                $is_next_page = false;
                $fail['message'] = 'Error while get whatshot';
                $fail['error'] = $getFeaturedContent;
                $status = Log::FAIL;
            } else {
                $status = Log::SUCCESS;
                $data = array_key_exists('data', $getFeaturedContent) ? $getFeaturedContent->data : array($getFeaturedContent);
                $attribut = array_key_exists('attributes', $getFeaturedContent) ? $getFeaturedContent->attributes : array();

                foreach($data as $FeaturedContent) {
                    $FeaturedContentModel = FeaturedContent::updateOrCreate([
                        'id' => $FeaturedContent->id
                    ],[
                        'order_position' => $FeaturedContent->order->position,
                        'order_name' => $FeaturedContent->order->name,
                        'schedule_start' => $FeaturedContent->schedule_start,
                        'schedule_end' => $FeaturedContent->schedule_end,
                        'type_tag' => $FeaturedContent->type_tag,
                        'tag' => json_encode($FeaturedContent->tag),
                        'news' => json_encode($FeaturedContent->news),
                        'title' => $FeaturedContent->title,
                        'url' => $FeaturedContent->url,
                        'desc' => $FeaturedContent->desc,
                        'image' => $FeaturedContent->image->real,
                        'image_secondary' => $FeaturedContent->image_secondary->real,
                        'status' => 1,
                        'last_fetch_at' => '2019-04-04 17:08:47'
                    ]);

                    if($FeaturedContentModel) {
                        $success++;
                    }else{
                        $fail['message'] = 'Error while save featured content';
                        $fail['error'] = $FeaturedContentModel;
                    }

                    $total++;
                }

                if(isset($attribut->next_page)){
                    $is_next_page = true;
                    $page = $attribut->next_page;
                    $this->newshub->page($page);
                }else{
                    $is_next_page = false;
                }
            }
        }

        // report
        $log->update_log($logId,[
            'total' => $total,
            'total_success' => $success,
            // 'total_fail' => ,
            'log_fail' => json_encode($fail),
            'ref_url' => $this->newshub->get_uri(),
            'status' => Log::SUCCESS,
        ]);

        $this->info("Get whatshot Success !");
        $headers = ['total data', 'success', 'fails', 'ref url'];
        $tables = [$total, $success, json_encode($fail), $this->newshub->get_uri()];
        $this->table($headers, [$tables]);
    }
}