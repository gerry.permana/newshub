<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = ['id','title','description','schedule_start','schedule_end','place','image','full_url','url','flag','type','tag','status'];

  /**
   * date mutator
   */
  protected $dates = ['schedule_start','schedule_end'];
}
