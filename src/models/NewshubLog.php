<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewshubLog extends Model
{
  /**
   * Newshub Log Table
   */
   protected $table='newshub_logs';

   /**
    *
    */
  protected $fillable = ['section','total','total_success','total_fail','log_fail','ref_url','status'];

}
