<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Whats_hot extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'title', 'order', 'schedule', 'headtorial', 'headtorial_schedule_end', 'image_real', 'url', 'flag', 'status'];
}
