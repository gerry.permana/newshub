<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Creator extends Model
{
    /**
     * fillable column
     */
    protected $fillable = ['id', 'name', 'url', 'youtube', 'instagram', 'facebook', 'twitter', 'image', 'image_cover', 'status'];
}
