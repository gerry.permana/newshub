<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeaturedContent extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','order_position','order_name','schedule_start','schedule_end','type_tag','tag','news','title','url','desc','image','image_secondary','status','last_fetch_at'];
}
