<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Instagram extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'user_id', 'image_thumbnail', 'image_low', 'image_standard', 'caption', 'link', 'has_carousel', 'created_time', 'status', 'created_at', 'updated_at'];
}
