<?php

namespace Lanuma\Kly;

use Lanuma\Kly\Http\ApiRequest;
use Lanuma\Kly\Http\NewshubException;
use Lanuma\Kly\Newshub\Log;

class Newshub
{
    /**
     * Instance of request
     */
    protected $newshub;

    /**
     * Newshub api url if config not found
     *
     */
    protected $api_url = 'https://www.newshub.id/api/';

    /**
     * Newshub token if config not found
     * this token used for development purpose only
     */
    protected $token;

    /**
     * kategori
     */
    protected $category;

    /**
     * id
     */
    protected $id;

    /**
     * page
     */
    protected $page;

    /**
     * limit
     */
    protected $limit;

    /**
     * orderby
     */
    protected $orderby;

    /**
     * headline
     */
    protected $headline;

    /**
     * last_update
     */
    protected $last_update;

    /**
     * custom parameter
     *
     */
    protected $param = array();

    protected $formated_uri;

    public function __construct($config = array())
    {
        $this->newshub = new ApiRequest([
            'base_uri' => ( null !== config('newshub.api_url') ) ? config('newshub.api_url') : $this->api_url,
            'timeout' => 10,
        ]);

        if(isset($config['token']))
            $this->token = $config['token'];
    }

    public function category($kategori)
    {
        $this->category = $kategori;

        return $this;
    }

    public function tag()
    {
        $this->category = 'tag';

        return $this;
    }

    public function banner()
    {
        $this->category = 'banner';

        return $this;
    }

    public function news()
    {
        $this->category = 'news';

        return $this;
    }

    public function whatshot()
    {
        $this->category = 'whatshot';

        return $this;
    }

    public function featured_content()
    {
        $this->category = 'featured';

        return $this;
    }

    public function event()
    {
        $this->category = 'event';

        return $this;
    }

    public function customer_service()
    {
        $this->category = 'customer-service';

        return $this;
    }

    public function videos()
    {
        $this->category = 'video';

        return $this;
    }

    public function creator_profile()
    {
        $this->category = 'creator';

        return $this;
    }

    public function interactive_content()
    {
        $this->category = 'interactive-content';

        return $this;
    }

    public function id($id)
    {
        $this->id = $id;

        return $this;
    }

    public function token($token)
    {
        $this->token = $token;

        return $this;
    }

    public function limit($limit)
    {
        $this->limit = $limit;

        return $this;
    }

    public function page($page)
    {
        $this->page = $page;

        return $this;
    }

    public function orderby($orderby)
    {
        $this->orderby = $orderby;

        return $this;
    }

    public function last_update($last_update)
    {
        $this->last_update = $last_update;

        return $this;
    }

    public function where($key = FALSE, $value = FALSE)
    {
        if(is_array($key))
        {
            foreach($key as $item => $val)
            {
                $this->param[$item] = $val;                
            }

            return $this;            
        }

        $this->param[$key] = $value;

        return $this;
    }

    public function get_uri()
    {
        return $this->formated_uri;
    }

    public function get($DEBUG = FALSE)
    {
        $this->token = $token = ( null !== config('newshub.token') ) ? config('newshub.token') : $this->token;

        $formated_uri = $this->category . '/';
        $formated_uri .= isset($this->id) ? $this->id . '/' : '';

        if($token)
            $arg['token'] = $this->token;

        if(!empty($this->limit))
            $arg['limit'] = $this->limit;

        if(!empty($this->page))
            $arg['page'] = $this->page;

        if(!empty($this->orderby))
            $arg['orderby'] = $this->orderby;

        if(!empty($this->last_update))
            $arg['last_update'] = $this->last_update;

        if(isset($arg))
            $formated_uri .= '&' . http_build_query($arg, '', '&');

        if(!empty($this->param))
            $formated_uri .= http_build_query($this->param, '', '&');

        $formated_uri .= '&nocache=' . microtime(true);

        $this->formated_uri = $formated_uri;

        try{

            if($DEBUG === TRUE) {
                return $formated_uri;
            }else{
                return json_decode($this->newshub->get($formated_uri));
            }
            
        } catch (ApiRequestException $e) {
            	echo $e;
        }
        
        
    }
}
