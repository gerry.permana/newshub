<?php

namespace Lanuma\Kly\Http;

use GuzzleHttp\Client;

class ApiRequest
{
    protected $base_uri;

    protected $timeout = 30;

    protected $client;

    public function __construct($config = array())
    {
        $this->client = new Client($config);
    }

    public function get($url)
    {
        $response = $this->client->request('GET', $url);
        $httpCode = $response->getStatusCode();
	$reason = $response->getReasonPhrase();

	if($httpCode === 200) {
	    return (string) $response->getBody();
	}else{
	    return json_encode(array(
	        'error' => $httpCode,
		'reason' => $reason
	    ));
	}
    }

    public function post($url, $data = array())
    {
        return $this->httpRequest($url, 'POST', $data);
    }

}
