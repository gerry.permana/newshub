<?php

namespace Lanuma\http;

use GuzzleHttp\Exception\RequestException;

class ApiRequestException extends RequestException {}
