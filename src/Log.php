<?php

namespace Lanuma\Kly\Newshub;

use App\NewshubLog;

class Log
{
	protected $s_time;

	protected $e_time;

	/**
	 * Indicates that process failed
	 */
	const FAIL = -1;

	/**
	 * Indicates that process success
	 */
    const SUCCESS = 0;

    /**
	 * Indicates that process runnning
	 */
    const RUNNING = 1;

    /**
     * Newshub section
     */
    const SECTION_NEWS = 1;
	const SECTION_PHOTONEWS = 2;
	const SECTION_VIDEO = 3;
	const SECTION_CATEGORY = 4;
	const SECTION_TAG = 5;
	const SECTION_TODAY_TAG = 6;
	const SECTION_WHATSHOT = 7;
	const SECTION_BANNER = 8;
	const SECTION_FBINFO = 9;
	const SECTION_PAGEVIEW = 10;
	const SECTION_HASHTAG = 11;
	const SECTION_POPULAR = 12;
	const SECTION_QUOTE = 13;
	const SECTION_EVENT = 14;
	const SECTION_FEATURE = 15;
	const SECTION_PROFILE = 16;
	const SECTION_CREATOR = 17;
	const SECTION_CUSTOMER_SERVICE = 18;
	const SECTION_INTERACTIVE_CONTENT = 19;

	public function start_time()
	{
		$this->e_time = microtime(true);

		return $this;
	}

	public function end_time()
	{
		$this->e_time = microtime(true);

		return $this;
	}

	/**
	 * Check Running Section in Log
	 */
	public function check_running_status($section)
	{
		$running = NewshubLog::where('section', $section)
		                       ->where('status', self::RUNNING)
		                       ->orderBy('created_at', 'desc')->first();

		return $running;
	}

	public function check_last_success($section)
	{
		$success = NewshubLog::where('section', $section)
		                     ->where('status', self::SUCCESS)
		                     ->orderBy('created_at', 'desc')->first();

        return $success;
	}

	public function create_running_log($section)
	{
		$running = NewshubLog::Create(['section' => $section, 'status' => self::RUNNING]);

		return $running->id;
	}

	 public function update_log($id, $data = array())
	{
		$update = NewshubLog::updateOrCreate([
			'id' => $id
		],$data);
	} 

}
