<?php 

use Orchestra\Testbench\TestCase;
use Lanuma\Kly\Newshub;

class NewshubTest extends TestCase
{

	public function testBannerOutput()
	{
		$newshub = new Newshub(['token' => env('NEWSHUB_TOKEN')]);
		
		$result = $newshub->banner()->get();
                //print_r($result);
		$this->assertArrayHasKey('data', (array) $result);
	}

	public function testTagOutput()
	{
		$newshub = new Newshub([
			'token' => env('NEWSHUB_TOKEN','')
		    ]);
		$result = $newshub->tag()
		                  ->limit(10)
		                  ->page(1)
		                  ->get();

		// print_r($result);

		$this->assertArrayHasKey('data', (array) $result);
	}
}
