<?php

use Orchestra\Testbench\TestCase;
use Lanuma\Kly\Http\ApiRequest;
// use Lanuma\Kly\HttpNew\ApiRequest;

class OutputTest extends TestCase
{
    protected $newshub;

    public function testTrue()
    {
        $this->assertTrue(true);
    }

    public function testOutput()
    {
        $newshub = new ApiRequest([
            'base_uri' => 'https://www.newshub.id/api/',
            'timeout' => 10,
        ]);

        $newshub = json_decode($newshub->get('banner/&token=3d7e18bf5d60838314682e45af6dde8e&limit=20&page=1'));
        // print_r((array) $newshub);
        $this->assertArrayHasKey('data', (array) $newshub);
    }

}
